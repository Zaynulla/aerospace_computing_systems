import sys
import yaml

if len(sys.argv) < 2:
    print("Введите: python req_classes.py <класс 1> <класс 2>")
    sys.exit(1)

new_elements = sys.argv[1:]

config_file_path = "config.yml"

with open(config_file_path, 'r') as file:
    config = yaml.safe_load(file)

config['services']['classifier']['req_classes'] = new_elements

with open(config_file_path, 'w') as file:
    yaml.dump(config, file)

print("классы обновлены")
