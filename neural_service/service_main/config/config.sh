#!/bin/bash

if [ $# -lt 2 ]; then
    exit 1
fi

new_elements=("$@")

python req_classes.py "${new_elements[@]}"
