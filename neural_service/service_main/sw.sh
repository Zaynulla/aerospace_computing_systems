 #!/bin/bash

python_file="create_swap_diff.py"

if [ -f "$python_file" ]; then
    python3 "$python_file"
else
    echo "Файл $python_file не найден."
fi
