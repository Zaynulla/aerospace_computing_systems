# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import os

from loguru import logger

from utils.convert_tflite import convert_model_tf
from utils.swap_diff_tf import apply_param_diff_h5

if __name__ == "__main__":
    list_name = ["weights/newbestmodel.h5", "weights/param_diff.h5"]

    apply_param_diff_h5("weights/bestmodel.h5", list_name[1], list_name[0])

    logger.debug("Swap diff model")

    convert_model_tf(list_name[0])

    logger.debug("new model successfully converted to tflite")

    [os.remove(f) for f in list_name if os.path.exists(f)]

    logger.debug("unnecessary files removed")
