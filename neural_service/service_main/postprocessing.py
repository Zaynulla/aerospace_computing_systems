# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

from omegaconf import OmegaConf

cfg = OmegaConf.load("config/config.yml")


def postprocess():
    classes = cfg["services"]["classifier"]["classes"]
    req_classes = cfg["services"]["classifier"]["req_classes"]
    list_index = open("data/result.txt", "r").read().split()
    label_list = [
        classes[int(ind_label)]
        for ind_label in list_index
        if classes[int(ind_label)] in req_classes
    ]

    return label_list


if __name__ == "__main__":
    print(postprocess())
