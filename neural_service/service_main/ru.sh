#!/bin/bash

python_file="app.py"

if [ -n "$1" ]; then
    python3 "$python_file" -i "$1"
else
    echo "Файл $python_file не найден."
fi

python_file="postprocessing.py"

if [ -f "$python_file" ]; then
    python3 "$python_file"
fi
