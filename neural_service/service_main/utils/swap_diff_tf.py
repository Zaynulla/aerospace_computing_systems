# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import argparse
import os
import shutil

import h5py
from loguru import logger

path_oldmodel = os.path.join("weights/bestmodel.h5")
path_diff = os.path.join("weights/param_diff.h5")

out_model = "weights/update_new_model.h5"

parser = argparse.ArgumentParser()
parser.add_argument(
    "-o",
    "--old_model",
    help="path to the old model",
    default=path_oldmodel,
)

parser.add_argument(
    "-d",
    "--diff_model",
    help="path to the diff model",
    default=path_diff,
)

parser.add_argument(
    "-n",
    "--new_model",
    help="path to the new model",
    default=out_model,
)
args = parser.parse_args()


def apply_param_diff_h5(old_model_path, param_diff_file_path, output_file_path):
    shutil.copy(old_model_path, output_file_path)

    with h5py.File(param_diff_file_path, "r") as param_diff_file:
        param_diff = {name: param_diff_file[name][()] for name in param_diff_file}

    with h5py.File(output_file_path, "r+") as output_model:
        for name, data in output_model.items():
            if name in param_diff:
                data[()] -= param_diff[name]


if __name__ in "__main__":
    apply_param_diff_h5(args.old_model, args.diff_model, args.new_model)
    logger.debug("Swap diff model")
