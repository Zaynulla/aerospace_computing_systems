# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import argparse
import os

import tensorflow as tf

path_model = os.path.join("weights/bestmodel.h5")

parser = argparse.ArgumentParser()
parser.add_argument(
    "-m",
    "--model",
    help="path to the model",
    default=path_model,
)
args = parser.parse_args()


def convert_model_tf(model_path):
    keras_model = tf.keras.models.load_model(model_path, compile=False)

    converter = tf.lite.TFLiteConverter.from_keras_model(keras_model)
    tflite_model = converter.convert()

    with open("weights/bestmodel.tflite", "wb") as f:
        f.write(tflite_model)


if __name__ == "__main__":
    convert_model_tf(args.model)
