tqdm==4.64.1
loguru==0.6.0
opencv-python==4.5.3.56
pandas==1.3.0
scikit-learn==1.0.2
tensorflow==2.7.0
tensorflow_addons==0.19.0
dataclasses==0.6
Pillow==9.4.0

pytest==7.1.2
pytest-cov==3.0.0
