/*
# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CANS_H
#define CANS_H

#include "stdint.h"
// #include "stdlib.h"
#include <linux/can.h>

// #include <linux/can/raw.h>

#include <bits/stdc++.h> //ЭТО ЗАГАДКА!!!
#include <sys/socket.h>
#include <sys/types.h>
// #include <sys/time.h>
// #include <netinet/in.h>

// #include <algorithm>
// #include <set>
// #include <time.h>
#include <net/if.h>
// #include <sys/ioctl.h>
#include <string.h>
#include <vector>

#include "cant.h"

// using namespace std;

// can cocket object
class cans
{
  private:
    int bytes;

  public:
    std::string name; // device name like "can0"
    int socan; // это собственно переменная handle сокета
    struct sockaddr_can addrcan;
    struct ifreq ifrcan;


    //подписка по ключам
    int KeyCount;
    int SubScount;
    std::vector <std::vector <int>> SubS;




    cans();


    void start(std::string DevName);
    void stop();

    void PrintSub(uint8_t ndx);
    void PrintSubS();


    int sendFrame(cant F);

    int recvFrame(cant *F, int mode, int timeout_sec);


    int sendFrameSub(cant F,uint8_t sub,bool FramePrint);

    int recvFrameSub(cant *F, int mode, int timeout_sec);

    ~cans();

}; // class cans

#endif // CANS_H
