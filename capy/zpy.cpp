#ifndef ZPY_H
#define ZPY_H

#include "cant.h"
/*
# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>

//*
extern "C"
{
    //    // include below each method you want to make visible outside
    cant *init() { return new cant(); }

    void InitId(cant *self){printf("iID \n"); self->InitId(); printf("iID2 \n");}

    void ClearIdStructure(cant *self){self->ClearIdStructure();}
    void AddIdStruct(cant *self,const char  Name[], uint8_t Length, bool Key){self->AddIdStruct(Name, Length, Key);}

    uint8_t getIdAttrSize(cant *self) { return self->getIdAttrSize(); }
    bool getLoadId(cant *self){return self->getLoadId();}

    void codeId(cant *self) { self->codeId(); }
    void decodeId(cant *self) { self->decodeId(); }
    bool setAttrByNdx(cant *self, int ndx, uint8_t Val){return self->setAttrByNdx(ndx, Val);}
    bool setAttrByName(cant *self, const char  Name[], uint8_t Val){return self->setAttrByName(Name, Val);}
    uint32_t getAttrByNdx(cant *self, int ndx){return self->getAttrByNdx(ndx);}
    uint32_t getAttrByName(cant *self,const char  Name[]){return self->getAttrByName(Name);}
    uint32_t getIdAttrMaxValByNdx(cant *self, int ndx){return self->getIdAttrMaxValByNdx(ndx);}

    uint32_t getIdAttrKeyByNdx(cant *self, int ndx){return self->getIdAttrKeyByNdx(ndx);}



    uint32_t getAdr(cant *self){return self->getAdr();}
    void setAdr(cant *self, uint32_t adr){self->setAdr(adr);}


    void test(cant *self, const char a[]){
           //printf("\n a= %s \n",a);
    self->test(a);
    }//void test

}
//*/

/*
extern "C"
{
//    // include below each method you want to make visible outside
    Test* init(int k) {return new Test(k);}
    void setInt(Test *self, int k) {self->setInt(k);}
    int getInt(Test *self) {return self->getInt();}
}
//*/

#endif
