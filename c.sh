#!bin/bash

camera_file="camera.py"

if [ -f "$camera_file" ]; then
	python "$camera_file"
fi

sudo docker run -v $(pwd)/tests:/app/tests -v $(pwd)/data:/app/data planet_service_tflite
