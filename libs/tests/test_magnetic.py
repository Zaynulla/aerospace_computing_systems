# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pytest

from magnetic import B_dot_body, required_magnetic_moment

B_dot_body_test_data = [
    pytest.param([1, 0, 0], [1, 0, 0], [0, 0, 0], id="x rotation"),
    pytest.param([1, 0, 0], [0, 1, 0], [0, 0, 1], id="y rotation"),
    pytest.param([1, 0, 0], [0, 0, 1], [0, -1, 0], id="z rotation"),
    pytest.param([1, 0, 0], [5, 8, 1], [0, -1, 8], id="mixed rotation"),
    pytest.param([1, 0, 0], [0, 0, 0], [0, 0, 0], id="no rotation"),
]


@pytest.mark.parametrize(
    "magnetic_vector, sat_omega_body, expected_b_dot_vector", B_dot_body_test_data
)
def test_B_dot_body(magnetic_vector, sat_omega_body, expected_b_dot_vector):
    # все компоненты посчитанного вектора должны совпадать с соответствующими
    # компонентами ожидаемого вектора
    assert (
        B_dot_body(magnetic_vector, sat_omega_body) == np.array(expected_b_dot_vector)
    ).all()


required_magnetic_moment_test_data = [
    pytest.param(
        np.array([1, 0, 0]),
        2.0,
        np.array([-2, 0, 0]),
        id="positive x, positive B_DOT_K",
    ),
    pytest.param(
        np.array([0, 1, 0]),
        0.5,
        np.array([0, -0.5, 0]),
        id="positive y, positive B_DOT_K",
    ),
    pytest.param(
        np.array([0, 0, 1]),
        0.5,
        np.array([0, 0, -0.5]),
        id="positive z, positive B_DOT_K",
    ),
    pytest.param(
        np.array([0, 1, 0]),
        0.5,
        np.array([0, -0.5, 0]),
        id="positive y, positive B_DOT_K",
    ),
    pytest.param(
        np.array([0, 0, 1]),
        -2,
        np.array([0, 0, 2]),
        id="positive z, negative B_DOT_K",
    ),
    pytest.param(
        np.array([0, 0, 0]),
        2,
        np.array([0, 0, 0]),
        id="x = 0, y = 0, z = 0, positive B_DOT_K",
    ),
    # Add more test cases as needed
]


@pytest.mark.parametrize(
    ("B_dot_body", "B_DOT_K", "expected_magnetic_moment"),
    required_magnetic_moment_test_data,
)
def test_required_magnetic_moment(B_dot_body, B_DOT_K, expected_magnetic_moment):
    result = required_magnetic_moment(B_dot_body, B_DOT_K)
    assert np.array_equal(result, expected_magnetic_moment)
