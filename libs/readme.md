# Библиотеки для работы численной модели

[adcs.py](adcs.py) - система ориентации и стабилизации (Altitude determination and
control system).

[ballistic.py](ballistic.py) - баллистика.

[can_utils.py](can_utils.py) - утилиты для работы с шиной CAN.

[cstpu.py](cstpu.py) - СЭДУ (Солнечная энерго-двигательная установка/Cubesat solar
thermal propulsion).

[helpers.py](helpers.py) - вспомогательные функции.

[magnetic.py](magnetic.py) - магнитное поле Земли, магнитометр, электромагнитные
катушки.

[power_budget.py](power_budget.py) - энергобаланс, СЭП (система электропитания).
