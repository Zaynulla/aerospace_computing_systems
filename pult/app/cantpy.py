# This file is part of Aerospace Computing Systems.

# Aerospace Computing Systems is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later version.

# Aerospace Computing Systems is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Aerospace
# Computing Systems. If not, see <https://www.gnu.org/licenses/>.

class cant(object):
    Data = b""

    CanEFFflg = int(0x80000000)
    #     = CAN_EFF_FLAG; // 0x80000000U; // EFF/SFF is set in the MSB //
    IdSize = 29

    CAN_id = 0

    # // 29 bit CAN adress
    # // 10011111 00000000 00000001 00000001
    # // 87654321 87654321 87654321 87654321
    # // E  Flags Attrib   Reciver  Sender
    # // F  0..31 0...256  0...256  0...256
    # // F
    # // byte[3]  byte[2]  byte[1]  byte[0]
    CAN_ID_FILE_NAME = "cantid"

    err = False
    LoadId = False

    IdAttrName = []
    IdAttrLength = []
    IdAttrKey = []
    IdAttrValue = []
    IdAttrMask = []
    IdAttrShift = []
    IdAttrMaxVal = []

    dlc = 8

    def __init__(self, FN="cantid"):
        print("\n \n -------Start cant constructor!")

        global CAN_ID_FILE_NAME
        CAN_ID_FILE_NAME = FN
        print("--Stucture file name=", CAN_ID_FILE_NAME)

        #global IdAttrName
        self.IdAttrName = ["Flags", "Attribute", "Receiver", "Sender"]
        #global IdAttrLength
        self.IdAttrLength = [5, 8, 8, 8]
        #global IdAttrKey
        self.IdAttrKey = [0, 0, 1, 0]

        self.SetId(self.IdAttrName, self.IdAttrLength, self.IdAttrKey)
        print("--Stucture Set N=", self.getIdAttrSize() )
        self.PrintIdStructure()

        global LoadId
        LoadId = False
        print("--Stucture Set NN=", self.getIdAttrSize() )
        self.LoadIdFile(self.CAN_ID_FILE_NAME)
        print("--Stucture Set NNN=", self.getIdAttrSize() )
        self.PrintIdStructure()
        print("\n  --cant constructor Ok!")

    # def __init__(self):

    def InitId(self):
        print("\n Start InitId!")

        # print("InitId#1\n")
        # building structure

        #global IdAttrValue
        #global IdAttrMask
        #global IdAttrShift
        #global IdAttrMaxVal

        self.IdAttrValue = []
        self.IdAttrMask = []
        self.IdAttrShift = []
        self.IdAttrMaxVal = []

        s = 0
        Uno = 1
        N = len(self.IdAttrName)
        # print("N =", N)

        for i in range(N):
            s += self.IdAttrLength[i]

        if s > self.IdSize:
            print(
                "CANT ERROR:(s=", s, ") > (IdSize=", self.IdSize, ") ; Fields count=", N
            )
            self.PrintIdStructure()
            exit(101)
        # print("s =", s)
        # init Values
        # print("\n InitId#2 \n")

        for i in range(N):
            self.IdAttrValue.append(0)
            self.IdAttrShift.append(0)
            self.IdAttrMask.append(Uno)
            s = 2
            for j in range(1, self.IdAttrLength[i]):
                s *= 2

            # print("MaxVal s=", s)
            self.IdAttrMaxVal.append(s)

        # building mask
        # print("\n InitId#3\n")
        # print("UNO=", bin(Uno))

        for i in range(N):
            # print(i, " IdAttrLength[i]=", IdAttrLength[i])
            for k in range(self.IdAttrLength[i] - 1):
                self.IdAttrMask[i] = self.IdAttrMask[i] << 1
                self.IdAttrMask[i] |= Uno
                # print("k=", k, " IdAttrMask[i]", bin(IdAttrMask[i]))

            # print("\n ", i, " IdAttrMask[i] ", bin(IdAttrMask[i]))
            for j in range((i + 1), N):
                for q in range(self.IdAttrLength[j]):
                    self.IdAttrShift[i] += 1

            self.IdAttrMask[i] = self.IdAttrMask[i] << self.IdAttrShift[i]
            self.IdAttrMask[i]
            # print(
            #     "finish: ",
            #     i,
            #     " IdAttrMask[i] ",
            #     f"{IdAttrMask[i]:036_b}".replace("_", " "),
            # )

        self.codeId()

    # print("InitId#4 \n InitId OK!!!!\n")

    # def InitId()

    def ClearIdStructure(self):
        # lib.ClearIdStructure(self.obj)
        print("\n ---- Start ClearIdStructure!")
        #global IdAttrName
        #global IdAttrLength
        #global IdAttrKey

        self.IdAttrName = []
        self.IdAttrLength = []
        self.IdAttrKey = []

    # def ClearIdStructure(self):

    def AddIdStruct(self, Name, Length, Key):
        # lib.AddIdStruct(self.obj, Name, Length, Key)
        #print("AddIdStruct !")
        #global IdAttrName
        #global IdAttrLength
        #global IdAttrKey
        self.IdAttrName.append(Name)
        self.IdAttrLength.append(Length)
        self.IdAttrKey.append(Key)
        # print("NNN=", len(IdAttrName))

    # def AddIdStruct(self, Name, Length, Key):

    def getIdAttrSize(self):
        return len(self.IdAttrName)

    def getLoadId(self):
        return LoadId

    def codeId(self):
        #print("\n ---Start codeId!")
        N = len(self.IdAttrName)
        #print("N=", N)
        Id = 0  # uint32_t

        for i in range(N):
            # print(
            #     "i=",
            #     i,
            #     " self.IdAttrValue[i]=",
            #     self.IdAttrValue[i],
            #     " self.IdAttrShift[i]=",
            #     self.IdAttrShift[i],
            # )
            S = self.IdAttrValue[i] << self.IdAttrShift[i]
            Id |= S
            # print("S=", f"{S:036_b}".replace("_", " "))
            # print("Id=", f"{Id:036_b}".replace("_", " "))

        self.CAN_id = Id | self.CanEFFflg
        # print("CanEFFflg=", f"{self.CanEFFflg:036_b}".replace("_", " "))
        # print("CAN_id=", f"{self.CAN_id:036_b}".replace("_", " "))

        # print("codeId Ok !")

    # def codeId(self):

    def decodeId(self):
        print("\n ----Start decodeId!")
        # print("CAN_id=", f"{self.CAN_id:036_b}".replace("_", " "))
        N1 = self.getIdAttrSize()

        Id1 = self.CAN_id
        for i in range(N1):
            S1 = Id1 & self.IdAttrMask[i]  # uint32_t
            self.IdAttrValue[i] = S1 >> self.IdAttrShift[i]
        # self.print_frame()
        # print("\n decodeId Ok!")

    # def decodeId(self):

    def setAttrByNdx(self, ndx, Val):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            if Val > self.IdAttrMaxVal[ndx]:
                self.IdAttrValue[ndx] = 0
                return 0
            else:
                self.IdAttrValue[ndx] = Val
                return 1
        else:
            return 0

    # def setAttrByNdx(self, ndx, Val):

    def setAttrByName(self, Name, Val):
        if Name in self.IdAttrName:
            ndx = self.IdAttrName.index(Name)
            if Val > self.IdAttrMaxVal[ndx]:
                self.IdAttrValue[ndx] = 0
                return 0
            else:
                self.IdAttrValue[ndx] = Val
                return 1
        else:
            print("WRONG IdAttrName!!!")
            exit(102)
            return 0

    # def setAttrByName(self, Name, Val):

    def getAttrByNdx(self, ndx):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            return self.IdAttrValue[ndx]
        else:
            print("getIdAttrValueByNdx WRONG Ndx!!!")
            exit(103)
            return 0

    # def getAttrByNdx(self, ndx):

    def getAttrByName(self, Name):
        if Name in self.IdAttrName:
            ndx = self.IdAttrName.index(Name)
            return self.IdAttrValue[ndx]
        else:
            print("WRONG IdAttrName!!!")
            exit(102)
            return 0

    # def getAttrByName(self, Name):

    def getIdAttrMaxValByNdx(self, ndx):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            return self.IdAttrMaxVal[ndx]
        else:
            print("getIdAttrMaxValByNdx WRONG Ndx!!!")
            exit(103)
            return 0

    # def getIdAttrMaxValByNdx(self, ndx):

    def getIdAttrKeyByNdx(self, ndx):
        if (ndx >= 0) and (ndx < self.getIdAttrSize()):
            return self.IdAttrKey[ndx]
        else:
            print("getIdAttrKeyByNdx WRONG Ndx!!!")
            exit(103)
            return 0

    # def getIdAttrKeyByNdx(self, ndx):

    def getAdr(self):
        # print("Start getAdr")
        return self.CAN_id | self.CanEFFflg

    def setAdr(self, adr):
        # print("\n ----Start setAdr")
        self.CAN_id = adr

    def test(self, s):
        print("tets")

    def SetId(self, NM, LN, KY):
        print("\n ----Start SetId")
        print("SetId = len(NM)", len(NM))
        if len(NM) == len(LN) == len(KY):
            self.ClearIdStructure()
            # print("getIdAttrSize=", self.getIdAttrSize())

            for i in range(len(NM)):
                print(NM[i], " ", LN[i], " ", KY[i])
                self.AddIdStruct(bytes(NM[i], "ascii"), int(LN[i]), int(KY[i]))
            print("InitID")

            self.InitId()
            # print("getIdAttrSize=", self.getIdAttrSize())

            for i in range(0, self.getIdAttrSize()):
                print("i=", i, " maxVal=", self.getIdAttrMaxValByNdx(i))
        else:
            print("CANT ERROR!!! len(NM)!=len(LN)")
        print("SetId Ok")

    # def SetId(self, NM, LN, KY):

    def setData(self, new_data):
        d = str(new_data)
        # print("Set data=", d)
        if len(d) > 8:
            d = d[:8]
        if len(d) < 8:
            # print("len(data)=", len(d))
            for j in range(len(d), 8):
                d += chr(0)
            # print("now data=", d)

        cant.Data = bytes(d, "ascii")

    # def setData(self, new_data):

    def print_frame(self):
        id = []
        for i in range(self.getIdAttrSize()):
            id.append(self.getAttrByNdx(i))
        print("frm id=", id, " data=", self.Data)

    # def print_frame(self):

    def LoadIdFile(self, name):

        try:
            with open(name) as protocol_file:
                lines = protocol_file.readlines()

            global LoadId
            LoadId = True

            print("cant.py Load LINES")
            print(lines)
            print("load LINES OK")

            field_names1, field_lengths1, field_keys1 = zip(
                *(line.split() for line in lines)
            )

            print(field_names1, field_lengths1, field_keys1)
            print("SetId")
            self.SetId(field_names1, field_lengths1, field_keys1)
            print("SetId OK")
        except FileNotFoundError:
            print(f"Запрашиваемый файл { name } не найден")

    # def LoadIdFile(self, name):

    def PrintIdStructure(self):

        N = len(self.IdAttrName)
        #print("Print Id structure N=",len(IdAttrName))
        #print("Print Id structure IdAttrLength=",len(IdAttrLength))
        #print("Print Id structure IdAttrKey N=",len(IdAttrKey))
        #print("Print Id structure IdAttrMaxVal N=",len(IdAttrMaxVal))
        for i in range(N):
            print("Field#",i," Name=",self.IdAttrName[i]," Len=",self.IdAttrLength[i]," Key=",self.IdAttrKey[i],"  MaxVal=",self.IdAttrMaxVal[i])

    # def PrintIdStructure():

    def print_attr_list(self):
        pass
