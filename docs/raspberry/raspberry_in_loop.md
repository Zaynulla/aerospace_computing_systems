# Подключение Raspberry в цикл имитационного моделирования

## Установка Docker на Raspberry Pi

1. Установка Docker, Docker-compose

    ```bash
    sudo apt update && sudo apt install docker docker-compose
    ```

1. Сборка образа

    ```bash
    docker-compose -f compose.obc.yaml build
    ```

## Открытие порта на сервере численного моделирования КА и внешней среды

1. На ноутбуке/ПК открыть порт `54701` разрешив доступ только от `rpi1.local` (или любое
   другое имя/IP устройства, которое будет выступать в роли моста между физическим CAN и
   TCP):

    ```bash
    sudo iptables -A INPUT -p tcp --dport 54701 -s rpi1.local -j ACCEPT
    sudo iptables -A INPUT -p tcp --dport 54701 -j DROP
    ```

1. Проверить доступность порта ноутбука/ПК с raspberry (`pc_hostname` необходимо
   заменить на hostname вашего ПК):

    ```bash
    sudo apt install nmap
    nmap -p 54701 pc_hostname.local

    # или
    # sudo apt install telnet
    # telnet pc_hostname.local 54701
    # Можно даже добавить следующие строки в строке приглашения
    # GET / HTTP/1.1
    # {'bitrate': 500000}
    # И получить html страницу с параметрами подключения
    ```

## Последовательность запуска численной модели и Raspberry

1. Запустите имитационную модель

    ```bash
    docker compose down --volumes && docker compose up
    ```

1. Дождитесь вывода сообщения об успешном запуске сервиса `can_server`

1. Запустите docker контейнер на raspberry

    ```bash
    docker-compose -f compose.obc.yaml up
    ```

## Настройка raspberry для работы в качестве моста CAN/TCP

1. Создайте виртуальное окружение (за основу взята эта
   [инструкция](https://raspberrypi-guide.github.io/programming/create-python-virtual-environment#installing)).

    ```bash
    sudo apt install -y python3-venv
    python -m venv venv
    ```

1. Активируйте виртуальное окружение:

    ```bash
    . venv/bin/activate
    ```

1. Установите зависимости:

    ```bash
    python -m pip install -r can_bridge/requirements.txt
    ```

1. Запустите скрипт моста:

    ```bash
    python can_bridge/bridge.py
    ```
