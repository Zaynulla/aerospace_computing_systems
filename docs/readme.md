# Документация по проекту

[numerical_simulation](numerical_simulation) - описание численной модели КА.

[foss_license_selection](foss_license_selection) - выбор свободной лицензии.

[guidelines](guidelines) - рекомендации по написанию кода на разных языка
программирования.

[raspberry](raspberry) - документация по настройке raspberry.

[usage_examples](usage_examples) - примеры тестирования информационной системы
«Земля–Борт» программного комплекса UEMKA.

[install_docker.md](install_docker.md) - документация по установке Docker на Raspberry.

[repository_mirror.md](repository_mirror.md) - работа с зеркалированным репозиторием.

[main.md](main.md) - точка входа для Pandoc, документ формирующий структуру из отдельных
файлов markdown.

Прочие не markdown файлы необходимы для автоматической конвертации с помощью Pandoc
markdown документов в docx.
